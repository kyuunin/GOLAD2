package golad.rules;

import golad.boards.BoardIterator;

public class CarykhRule implements Rule {


	public State nextState(BoardIterator it) {
		int x = 0;
		State s;
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0)
					continue;
				s = it.neighbor(i, j);
				if (s == null)
					continue;
				x += s.getPoints(-1, it);
			}
		}
		if (it.get().getPoints(-1, it) == 1) {
			if (x == 2 || x == 3)
				return it.get();
		} else if (x == 3) {
			int y = 0;
			x = 0;
			for (int i = -1; i < 2; i++) {
				for (int j = -1; j < 2; j++) {
					if (i == 0 && j == 0)
						continue;
					s = it.neighbor(i, j);
					if (s == null)
						continue;
					x += s.getPoints(0, it);
					y += s.getPoints(1, it);
				}
			}
			if (x > y)
				return State.TEAM1;
			if (y > x)
				return State.TEAM2;
			return State.ALIVE;
		}
		return State.DEAD;
	}

}
