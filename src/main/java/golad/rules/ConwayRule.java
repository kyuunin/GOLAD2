package golad.rules;

import golad.boards.BoardIterator;

public class ConwayRule implements Rule {


	public State nextState(BoardIterator it) {
		int x = 0;
		State s;
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0)
					continue;
				s = it.neighbor(i, j);
				if (s == null)
					continue;
				x += s.getPoints(-1, it);
			}
		}
		if (it.get().getPoints(-1, it) == 1) {
			if (x == 2)
				return State.ALIVE;
		}
		if (x == 3)
			return State.ALIVE;
		return State.DEAD;
	}

}
