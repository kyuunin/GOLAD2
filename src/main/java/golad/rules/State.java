package golad.rules;

import golad.boards.BoardIterator;

/**
 * This Interface defines the State of a Cell
 * 
 * @author kyuunin
 *
 */
public interface State {

	/**
	 * Returns the Points for this State
	 * 
	 * @param side
	 *            the side of which the Points should be returned<br>
	 *            -1 for the total Points
	 * @param it
	 *            the Position of the Cell
	 * @return the Points
	 */
	public int getPoints(int side, BoardIterator it);

	/**
	 * this State is describes a Living Cell in Conway's Game of Life.<br>
	 * -1 get 1 Point
	 */
	public static final State ALIVE = new State() {
		public int getPoints(int side, BoardIterator it) {
			return side == -1 ? 1 : 0;
		}

		@Override
		public String toString() {
			return "x";
		}
	};

	/**
	 * this State is describes a Dead Cell in Conway's Game of Life.<br>
	 * -1 get 0 Point
	 */
	public static final State DEAD = new State() {
		public int getPoints(int side, BoardIterator it) {
			return 0;
		}

		@Override
		public String toString() {
			return "-";
		}
	};

	/**
	 * this State is describes a Cell of Player 1 in Game of Life and Death.<br>
	 * -1 get 1 Point<br>
	 * 0 get 1 Point
	 */
	public static final State TEAM1 = new State() {
		public int getPoints(int side, BoardIterator it) {
			return side == 0 || side == -1 ? 1 : 0;
		}

		@Override
		public String toString() {
			return "r";
		}
	};
	/**
	 * this State is describes a Cell of Player 2 in Game of Life and Death.<br>
	 * -1 get 1 Point<br>
	 * 1 get 1 Point
	 */
	public static final State TEAM2 = new State() {
		public int getPoints(int side, BoardIterator it) {
			return side == 1 || side == -1 ? 1 : 0;
		}

		@Override
		public String toString() {
			return "b";
		}
	};
}
