package golad.rules;

import golad.boards.BoardIterator;

/**
 * Rules describes the Rules of Game of Life
 * 
 * @author kyuunin
 *
 */
public interface Rule {
	/**
	 * This Calculates the new State for a given Cell
	 * 
	 * @param it
	 *            The BoardIterator defines the Position of the Cell that should
	 *            be changed
	 * @return the following State
	 */
	public State nextState(BoardIterator it);

}
