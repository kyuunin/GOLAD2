package golad.rules;

import golad.boards.BoardIterator;

public class ConquerRule implements Rule {

	public State nextState(BoardIterator it) {
		int x = 0;
		int y = 0;
		int z = 0;
		State s;
		for (int i = -1; i < 2; i++) {
			for (int j = -1; j < 2; j++) {
				if (i == 0 && j == 0)
					continue;
				s = it.neighbor(i, j);
				if (s == null)
					continue;
				x += s.getPoints(-1, it);
				y += s.getPoints(0, it);
				z += s.getPoints(1, it);
			}
		}
		s = it.get();
		if (s.getPoints(-1, it) == 1 && (x == 3 || x == 2)) {
			if (y == z)
				return s;
			if (y == 3)
				return State.TEAM1;
			if (z == 3)
				return State.TEAM2;
			return s;
		} else if (x == 3) {
			if (y > z)
				return State.TEAM1;
			if (z > y)
				return State.TEAM2;
			return State.ALIVE;
		}
		return State.DEAD;
	}

}
