package golad.boards;

import java.util.Iterator;

import golad.rules.State;

/**
 * this Interface describes an Iterator for Boards
 * 
 * @author kyuunin
 *
 */
public interface BoardIterator extends Iterator<State> {

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#next()
	 */
	public State next();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Iterator#hasNext()
	 */
	public boolean hasNext();

	/**
	 * returns the State of the Current Position
	 * 
	 * @return the State of the Current Position.<br>
	 *         this should not return null
	 */

	public State get();

	/**
	 * sets the State of the Current Cell
	 * 
	 * @param s
	 *            the new State of the Cell.<br>
	 *            if s is null this should return a {@link NullPointerException}
	 */
	public void set(State s);

	/**
	 * returns the State of a neighbor relative to the Current Cell
	 * 
	 * @param x
	 *            the vertical distance to the Current Cell
	 * @param y
	 *            the horizontal distance to the Current Cell
	 * @return the State of the neighbor.<br>
	 *         iff the neighbor doesn't exist this should return null
	 */
	public State neighbor(int x, int y);

}
