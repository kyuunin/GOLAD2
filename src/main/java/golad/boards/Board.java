package golad.boards;

import golad.rules.Rule;
import golad.rules.State;

/**
 * This Interface describes an Board
 * 
 * @author kyuunin
 *
 */
public interface Board extends Cloneable, Iterable<State> {

	/**
	 * Do an Iteration of Game of Life
	 * 
	 * @param r
	 *            the Rules for this Game of Life iteration
	 * @return the resulting board
	 */
	public Board doIteration(Rule r);

	/**
	 * Returns the state of the Cell(x,y)
	 * 
	 * @param x
	 *            the vertical Position
	 * @param y
	 *            the horizontal Position
	 * @return the current State of the Field<br>
	 *         null iff the Field doesn't exist
	 */
	public State getState(int x, int y);

	/**
	 * Sets the state of the Cell(x,y) to s
	 * 
	 * @param s
	 *            the new State
	 * @param x
	 *            the vertical Position
	 * @param y
	 *            the horizontal Position
	 * @return true iff something changed
	 */
	public boolean setState(State s, int x, int y);

	/**
	 * Returns the points for a given Side
	 * 
	 * @param side
	 *            -1 for the total Points
	 * @return the sum of all Points
	 */
	public int getPoints(int side);

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Iterable#iterator()
	 */
	public BoardIterator iterator();
}
