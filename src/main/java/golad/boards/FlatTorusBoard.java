package golad.boards;

import golad.rules.Rule;
import golad.rules.State;

public class FlatTorusBoard implements Board {

	private State[] states;
	private int height;
	private int width;

	public FlatTorusBoard(int height, int width) {
		this.height = height;
		this.width = width;
		states = new State[height * width];
	}

	private FlatTorusBoard(State[] states, int height, int width) {
		this.height = height;
		this.width = width;
		this.states = states;
	}

	@Override
	protected FlatTorusBoard clone() {
		return new FlatTorusBoard(states.clone(), height, width);
	}

	public FlatTorusBoard doIteration(Rule r) {
		// TODO
		FlatTorusBoard next = clone();
		BoardIterator thisIt = iterator();
		BoardIterator nextIT = next.iterator();
		while (thisIt.hasNext()) {
			thisIt.next();
			nextIT.next();
			nextIT.set(r.nextState(thisIt));
		}
		return next;
	}

	public State getState(int x, int y) {
		while (x < 0) {
			x += height;
		}
		x %= height;
		while (y < 0) {
			y += width;
		}
		y %= width;
		return states[x + y * height];
	}

	public boolean setState(State s, int x, int y) {
		while (x < 0) {
			x += height;
		}
		x %= height;
		while (y < 0) {
			y += width;
		}
		y %= width;
		states[x + y * height] = s;
		return true;

	}

	public int getPoints(int side) {
		int res = 0;
		BoardIterator it = this.iterator();
		State s;
		while (it.hasNext()) {
			if ((s = it.next()) == null)
				continue;
			res += s.getPoints(side, it);
		}
		return res;
	}

	public BoardIterator iterator() {
		return new BoardIterator() {
			private int p = -1;

			public void set(State s) {
				states[p] = s;

			}

			public State neighbor(int x, int y) {
				return getState(x + p % height, y + p / height);
				// return states[p + x + y * height];
			}

			public State get() {
				return states[p];
			}

			public State next() {
				p++;
				return get();
			}

			public boolean hasNext() {
				return p + 1 < states.length;
			}
		};
	}

}
