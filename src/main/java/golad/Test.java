package golad;

import java.util.Random;
import java.util.Scanner;

import golad.boards.Board;
import golad.boards.ClosedRectangleBoard;
import golad.controllers.Controller;
import golad.controllers.RandAI;
import golad.fillers.RandomFiller;
import golad.gamemodes.CreateMode2P;
import golad.gamemodes.GameMode;
import golad.rules.CarykhRule;
import golad.rules.Rule;
import golad.rules.State;

public class Test {

	public static final boolean ON = true;

	public static int h = 10;
	public static int w = 20;
	public static Scanner scan = new Scanner(System.in);
	public static Controller ai;

	public static void main(String[] args) throws InterruptedException {
		Board b = new ClosedRectangleBoard(h, w);
		Rule r = new CarykhRule();
		GameMode m = new CreateMode2P(b, r, true);
		ai = new RandAI(m, new Random(), h, w);
		fill(b);
		print(b);
		while (true) {
			Board b2 = act(m);
			if (b2 != null) {
				b = b2;
			}
			System.out.println(b);
			print(b);
		}
	}

	public static Board act(GameMode m) {
		System.out.println(m.getPlayer());
		if (m.getPlayer() == 0) {
			System.out.println("What do you want to do?[add/undo/accept]");
			String in = scan.nextLine();
			switch (in) {
			case "add":
				System.out.println("Choose Point");
				System.out.println(m.add(scan.nextInt(), scan.nextInt()));
				scan.nextLine();
				return null;
			case "undo":
				System.out.println(m.remove());
				return null;
			case "accept":
				return m.accept();
			}
			return null;
		} else {
			ai.act();
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return m.getBoard();
		}

	}

	public static void fill(Board b) {
		new RandomFiller(h, w, new State[] { State.ALIVE, State.DEAD, State.TEAM1, State.TEAM2 },
				new double[] { Math.random(), Math.random(), Math.random(), Math.random() }, 1).fill(b);
	}

	public static void print(Board b) {
		System.out.printf("Player1%5d%nPlayer2%5d%n", b.getPoints(0), b.getPoints(1));
		for (int i = 0; i < h; i++) {
			for (int j = 0; j < w; j++) {
				System.out.print(b.getState(i, j));
			}
			System.out.println();
		}
		System.out.println();
	}

}
