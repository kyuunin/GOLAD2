package golad.gamemodes;

import golad.boards.Board;
import golad.rules.Rule;
import golad.rules.State;

public class CreateMode2P implements GameMode {

	private Board b;
	private Rule r;
	private boolean player;
	private int x;
	private int y;
	private State s;

	public CreateMode2P(Board b, Rule r, boolean player) {
		this.b = b;
		this.r = r;
		this.player = player;
	}

	public Board accept() {
		if (s != null) {
			player = !player;
			x = -1;
			y = -1;
			s = null;
			return b = b.doIteration(r);

		}
		return null;
	}

	public boolean remove() {
		if (s != null) {
			b.setState(s, x, y);
			x = -1;
			y = -1;
			s = null;
			return true;
		}
		return false;
	}

	public boolean add(int x, int y) {
		if (s == null) {
			s = b.getState(x, y);
			if (s == State.DEAD) {
				b.setState(player ? State.TEAM1 : State.TEAM2, x, y);
				if (b.getState(x, y) != null) {
					this.x = x;
					this.y = y;
					return true;
				}
			}
			s = null;
		}
		return false;
	}

	public boolean mode(int mode) {
		return false;
	}

	@Override
	public boolean canAccept() {
		return s != null;
	}

	@Override
	public int[] posModes() {
		return new int[] {};
	}

	@Override
	public State[] posStates() {
		return new State[] { State.DEAD };
	}

	@Override
	public Board getBoard() {
		return b;
	}

	@Override
	public int getPlayer() {
		return player ? 0 : 1;
	}

}
