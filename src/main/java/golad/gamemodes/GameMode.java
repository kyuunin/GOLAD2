package golad.gamemodes;

import golad.boards.Board;
import golad.rules.State;

/**
 * the GameMode describes the Actions and Possibilities the Game has.
 * 
 * @author kyuunin
 *
 */
public interface GameMode {
	/**
	 * 
	 * @return the Board on which is Played
	 */
	public Board getBoard();

	/**
	 * finished the current Turn and calls a instance of Game of Life defined in
	 * the Rules
	 * 
	 * @return the resulting Board. <br>
	 *         returns null iff the action dosen't succeed.
	 */
	public Board accept();

	/**
	 * removes the newest Command
	 * 
	 * @return true iff something on the Board changed
	 */
	public boolean remove();

	/**
	 * adds an Play Command to the Command list.
	 * 
	 * @param x
	 *            the vertical Position of the Cell that should be changed.
	 * @param y
	 *            the horizontal Position of the Cell that should be changed
	 * @return true iff the Command succeed
	 */
	public boolean add(int x, int y);

	/**
	 * changes the mode for the Commands
	 * 
	 * @param mode
	 *            the new Mode
	 * @return true iff mode changed
	 */
	public boolean mode(int mode);

	/**
	 * @return true iff {@link #accept()} will succeed
	 */
	public boolean canAccept();

	/**
	 * 
	 * @return an int Array of possible modes for {@link #mode(int)}
	 */
	public int[] posModes();

	/**
	 * 
	 * @return an Array of states that can be affect with {@link #add(int, int)}
	 */
	public State[] posStates();

	/**
	 * 
	 * @return the side of the active Player
	 */
	public int getPlayer();
}
