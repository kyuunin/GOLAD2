package golad.gamemodes;

import golad.boards.Board;
import golad.rules.Rule;
import golad.rules.State;

public class ConwayMode implements GameMode {
	private Board b;
	private Rule r;

	public ConwayMode(Board b, Rule r) {
		this.b = b;
		this.r = r;
	}

	public Board accept() {
		return b = b.doIteration(r);
	}

	public boolean remove() {
		return false;
	}

	public boolean add(int x, int y) {
		return false;
	}

	public boolean mode(int mode) {
		return false;
	}

	@Override
	public boolean canAccept() {
		return true;
	}

	@Override
	public int[] posModes() {
		return new int[] {};
	}

	@Override
	public State[] posStates() {
		return new State[] {};
	}

	@Override
	public Board getBoard() {
		return b;
	}

	@Override
	public int getPlayer() {
		return -1;
	}

}
