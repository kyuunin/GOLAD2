package golad.controllers;

import java.util.Random;

import golad.Test;
import golad.gamemodes.GameMode;

public class RandAI implements Controller {

	private GameMode m;
	private Random rand;
	private int height;
	private int width;

	public RandAI(GameMode m, Random rand, int height, int width) {
		super();
		this.m = m;
		this.rand = rand;
		this.height = height;
		this.width = width;
	}

	@Override
	public void act() {
		while (true) {
			switch (rand.nextInt(3)) {
			case 0:
				//TODO Test Code
				if (m.add(rand.nextInt(height), rand.nextInt(width)) && Test.ON) {
					try {
						Thread.sleep(200);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					Test.print(m.getBoard());
				}
				break;
			case 1:
				int[] modes = m.posModes();
				if (modes.length > 0) {
					m.mode(modes[rand.nextInt(modes.length)]);
				}
				break;
			case 2:
				if (m.canAccept()) {
					m.accept();
					return;
				}

				break;
			}

		}

	}

}
