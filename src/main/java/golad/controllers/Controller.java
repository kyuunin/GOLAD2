package golad.controllers;

/**
 * Controller describes the behavior of AIs and the actions of Players
 * 
 * @author kyuunin
 *
 */
public interface Controller {
	/**
	 * Let the Controller act.<br>
	 * At the end this should call accept of the Active GameMode.
	 */
	public void act();
}
