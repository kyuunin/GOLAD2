package golad.fillers;

import java.util.Random;

import golad.boards.Board;
import golad.rules.State;

public class RandomFiller implements Filler {

	private int height;
	private int width;
	private State[] states;
	private double[] prop;
	private Random rand;

	public RandomFiller(int height, int width, State[] states, double[] prop, long seed) {
		this.states = states;
		this.prop = prop;
		double sum = 0;
		for (int i = 0; i < prop.length; i++) {
			sum += prop[i];
		}
		for (int i = 0; i < prop.length; i++) {
			prop[i] /= sum;
		}
		this.height = height;
		this.width = width;
		rand = new Random(seed);
	}

	private State randState() {
		double tmp = rand.nextDouble();
		for (int i = 0; i < states.length; i++) {
			if (tmp < prop[i]) {
				return states[i];
			}
			tmp -= prop[i];
		}
		System.out.println(tmp);
		return states[states.length - 1];

	}

	public void fill(Board b) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				b.setState(randState(), i, j);
			}
		}
	}

}
