package golad.fillers;

import golad.boards.Board;

/**
 * Filler fill boards with an Initial Cells
 * 
 * @author kyuunin
 *
 */
public interface Filler {
	/**
	 * fills the Board
	 * 
	 * @param b
	 *            the Board that should be filled
	 */
	public void fill(Board b);
}
